#include "Perf_Model.h"

#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

PerfModel::PerfModel()
{
   _enabled = false;
   icount = 0;
   read_accesses = 0;
   write_accesses = 0;
   cache_hits = 0;
   cache_misses = 0;
   cache_evictions = 0;
   cache_hit_time.tv_sec = 0;
   cache_hit_time.tv_nsec = CACHE_HIT_COST;
   dram_reads = 0;
   dram_writes = 0;
   cache_miss_time.tv_sec = 0;
   cache_miss_time.tv_nsec = CACHE_MISS_COST;
   hash_computations = 0;
   hash_comp_time.tv_sec = 0;
   hash_comp_time.tv_nsec = HASH_COMP_COST;
}

PerfModel::~PerfModel()
{}

void PerfModel::RecordSimStart()
{
   gettimeofday(&start_time, NULL);
}

void PerfModel::RecordSimEnd()
{
   gettimeofday(&end_time, NULL);
}

void PerfModel::UpdateEventCounter(EventType event)
{
   if(!_enabled)
      return;

   switch(event)
   {
      case INST:
         icount++;
         break;

      case CORE_READ:
         read_accesses++;
         nanosleep(&cache_hit_time, NULL);
         break;

      case CORE_WRITE:
         write_accesses++;
         break;

      case CACHE_HIT:
         cache_hits++;
         break;

      case CACHE_MISS:
         cache_misses++;
         break;

      case CACHE_EVICTION:
         cache_evictions++;
         break;

      case HASH_COMP:
         hash_computations++;
         nanosleep(&hash_comp_time, NULL);
         break;

      case DRAM_READ:
         dram_reads++;
         nanosleep(&cache_miss_time, NULL);
         break;

      case DRAM_WRITE:
         dram_writes++;
         break;

      default:
         assert(false);
   }
}

void PerfModel::PrintSummary()
{
   // Compute total time elapsed
   int64_t total_us = (end_time.tv_sec - start_time.tv_sec)*1E6 + (end_time.tv_usec - start_time.tv_usec);

   // Cache Hit rate
   cache_hit_rate = (cache_hits * 1.0) / (cache_hits + cache_misses);

   cout << "*******************************************************\n";
   cout << "                 SIMULATOR STATISTICS                  \n";
   cout << "*******************************************************\n";
   cout << setw(30) << "CORE SUMMARY "          << " " << endl;
   cout << setw(30) << "Instructions Count "    << icount << endl;
   cout << setw(30) << "Read Accesses "         << read_accesses << endl;
   cout << setw(30) << "Write Accesses "        << write_accesses << endl;
   
   cout << setw(30) << "CACHE SUMMARY "         << " " << endl;
   cout << setw(30) << "Cache Hits "            << cache_hits << endl;
   cout << setw(30) << "Cache Misses "          << cache_misses << endl;
   cout << setw(30) << "Cache Evictions "       << cache_evictions << endl;
   cout << setw(30) << "Cache Hit Rate "        << cache_hit_rate << endl;
   
   cout << setw(30) << "DRAM SUMMARY "          << " " << endl;
   cout << setw(30) << "DRAM Reads "            << dram_reads << endl;
   cout << setw(30) << "DRAM Writes "           << dram_writes << endl;
   cout << setw(30) << "Hash Computations "     << hash_computations << endl;
   cout << setw(30) << "Total Time (us) "       << total_us << endl;
   cout << "*******************************************************\n";
   fflush(stdout);
}

