#ifndef PERF_MODEL_H
#define PERF_MODEL_H

#include <sys/time.h>
#include <stdint.h>
#include <cstdio>
#include <assert.h>
#include <string.h>
#include <map>

#include "Parameters.h"

class PerfModel
{
public:
   enum EventType
   {
      INST = 0,
      CORE_READ,
      CORE_WRITE,
      CACHE_HIT,
      CACHE_MISS,
      CACHE_EVICTION,
      HASH_COMP,
      DRAM_READ,
      DRAM_WRITE
   };

	PerfModel();
	~PerfModel();

   void EnableModels() { _enabled = true; }
   void DisableModels() { _enabled = false; }
   void RecordSimStart();
   void UpdateEventCounter(EventType event);
   void RecordSimEnd();
   void PrintSummary();
	
private:
  
   // Enable/Disable modeling
   bool _enabled;

   // Core
   uint64_t icount;
   uint64_t read_accesses;
   uint64_t write_accesses;
   
   // Cache
   uint64_t cache_hits;
   uint64_t cache_misses;
   uint64_t cache_evictions;
   float cache_hit_rate;
   struct timespec cache_hit_time;
   
   // DRAM
   uint64_t dram_reads;
   uint64_t dram_writes;
   struct timespec cache_miss_time;

   // Integrity
   uint64_t hash_computations;
   struct timespec hash_comp_time;

   // Overall runtime
   struct timeval start_time, end_time;
	
};

#endif
