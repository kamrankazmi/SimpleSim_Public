#include "SimpleDram.h"

SimpleDram::SimpleDram(int capacity, int blockSize)
{
	Capacity = capacity * 1024 * 1024; // Mega Bytes to Bytes
	BlockSize = blockSize;
	Blocks = Capacity / BlockSize;
	
	DRAM = new SimpleDramline [Blocks];
	debug = 0;
   
   // Initialize
   for(uint64_t i=0; i<Blocks; i++)
   {
      DRAM[i].addr = i;
   }
}

SimpleDram::~SimpleDram() 
{ 
}

void SimpleDram::Read(uint64_t addr, uint8_t* data_buf)
{
   uint64_t cl_num = addr / CACHELINE_SIZE;
   assert(cl_num < Blocks);

   SimpleDramline* dram_line = &DRAM[cl_num];
   assert(dram_line->data && data_buf);
   assert(dram_line->addr == cl_num);

   // Read data from DRAM model
   memcpy(data_buf, dram_line->data, CACHELINE_SIZE);
}


void SimpleDram::Write(uint64_t addr, uint8_t* data_buf)
{	
   uint64_t cl_num = addr / CACHELINE_SIZE;
   assert(cl_num < Blocks);

   SimpleDramline* dram_line = &DRAM[cl_num];
   assert(dram_line->data && data_buf);
   assert(dram_line->addr == cl_num);

   // Update data in DRAM model
   memcpy(dram_line->data, data_buf, CACHELINE_SIZE);
}
