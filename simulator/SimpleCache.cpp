#include "SimpleCache.h"

SimpleCache::SimpleCache(int capacity, int blockSize, int ways)
{
	Capacity = capacity;
	BlockSize =  blockSize;
	Ways = ways;
	
	Blocks = Capacity / BlockSize;
	Sets = Blocks / Ways;
	
	cache = new SimpleCacheline [Blocks];		
	evicted = new SimpleCacheline;

	numAccess = 0;	
}

SimpleCache::~SimpleCache() 
{ 
	delete []cache;
	delete evicted;
}

bool SimpleCache::Read(uint64_t addr, uint8_t* data_buf)
{
	numAccess++;
   assert(addr % CACHELINE_SIZE == 0);
	int set = (addr/CACHELINE_SIZE) % Sets;
	for (int j = 0; j < Ways; j++)
	{
		SimpleCacheline * current = cache + set * Ways + j;
		if (current->valid && current->tag == addr)
		{
			current->last_time = numAccess;
         memcpy(data_buf, current->data, CACHELINE_SIZE);
			return true;
		}
	}

   return false;
}

bool SimpleCache::Write(uint64_t addr, uint8_t* data_buf)
{	
   assert(addr % CACHELINE_SIZE == 0);
	SimpleCacheline * current;
   uint8_t buf[CACHELINE_SIZE];
	int set = (addr/CACHELINE_SIZE) % Sets;
   
   bool hit = Read(addr, buf);
   if(hit) // Line already present in cache. Set Dirty Bit
   {
      // Find where exactly this cacheline resides and update it.
      int hit_at = -1;
      for (int j = 0; j < Ways; j++)
      {
         current = cache + set * Ways + j;
         if (current->valid && current->tag == addr)
         {
            hit_at = set * Ways + j;
            break;
         }
      }
      assert(hit_at >= 0);
      current = cache + hit_at;
      memcpy(current->data, data_buf, CACHELINE_SIZE); // Write data in cache
      current->dirty = 1;
      return 0;   // No Eviction
   }

	numAccess--;		
	uint64_t least_recent_time = cache[set * Ways].last_time;
	int replacement = 0;
	for (int j = 0; j < Ways; j++)
	{
		current = cache + set * Ways + j;
		if (!current->valid)
		{
			replacement = j;
			break;
		}
		else if (current->last_time < least_recent_time)
		{
			replacement = j;
			least_recent_time = current->last_time;
		}
	}
	current = cache + set * Ways + replacement;
	*evicted = *current;
	*current = (SimpleCacheline) {1, addr, numAccess, 0};

   // Handle Data
   memcpy(current->data, data_buf, CACHELINE_SIZE); // Write new data in cache
   memcpy(data_buf, evicted->data, CACHELINE_SIZE); // Copy evicted data into the buffer

	if(evicted->valid && evicted->dirty)
      return true;   // Eviction. Evicted Address stored in "evicted->tag"
   else
      return false;   // No Eviction if line is Clean. Simply discard it with no writeback.
}


