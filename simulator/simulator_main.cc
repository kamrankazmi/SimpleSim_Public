#include "pin.H"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sys/time.h>
#include <assert.h>
#include <string.h>
#include <map>

#include "Parameters.h"
#include "Perf_Model.h"
#include "SimplePerfModel.h"
#include "SimpleCache.h"
#include "SimpleDram.h"
#include "MerkleTreeDram.h"
#include "MacTreeDram.h"

// Global Variables
/* ================================================================== */
bool _enable_models = false;

// lock serializes access to shared resources e.g. performance model.
PIN_LOCK lock;
volatile int _total_threads = 0;
volatile unsigned int _currently_running_thread = 0;
std::map<int, uint64_t> _inst_count_map;              // Used for simulating context switching.
std::map<int, uint64_t> _next_suspension_inst_count;  // Used for simulating context switching.
std::map<int, bool> _suspension_allowed;              // Used for simulating context switching.
std::map<int, bool> _alive_threads;                   // Used for simulating context switching.

// Page Table
std::map<uint64_t, uint64_t> _page_table;

// Performance Model
SimplePerfModel* _perf_model;

// Cache model
SimpleCache* _cache_model;

// DRAM model
#if (DRAM_TYPE == 1)  // Merkle Tree 
MerkleTreeDram* _dram_model;
#elif (DRAM_TYPE == 2) // MAC Tree
MacTreeDram* _dram_model;
#else
SimpleDram* _dram_model;
#endif

/* ================================================================== */

enum AccessType_t
{
   ACCESS_TYPE_LOAD,    // 0
   ACCESS_TYPE_STORE
};

/**********************************************************************
 * 
 * Helpers 
 *
 * ********************************************************************/
// Page Table Emulation
uint64_t VirtualToPhysicalAddress(uint64_t vaddr)
{
   // vaddr in the same page (e.g. 4K) are mapped to consecutive oaddrs
   assert(PAGE_SIZE > CACHELINE_SIZE);
   uint64_t page_idx = vaddr / PAGE_SIZE;
   uint64_t page_offset = vaddr % PAGE_SIZE;

   uint64_t paddr;
   std::map<uint64_t, uint64_t>::iterator VAIt = _page_table.find(page_idx);
   if (VAIt == _page_table.end())
   {
      _page_table.insert(pair<uint64_t, uint64_t>(page_idx, _page_table.size()));
      paddr = _page_table.size() - 1;
   }
   else
      paddr = VAIt->second;
   return paddr * PAGE_SIZE + page_offset;
}

/**********************************************************************
 * 
 * Analysis Rotuines 
 *
 * ********************************************************************/

// Note:  threadid+1 is used as an argument to the PIN_GetLock()
//        routine as a debugging aid.  This is the value that
//        the lock is set to, so it must be non-zero.

// This routine is executed every time a thread is created.
VOID ThreadStart(THREADID threadid, CONTEXT *ctxt, INT32 flags, VOID *v)
{
    PIN_GetLock(&lock, threadid+1);
    _total_threads++;
    _inst_count_map[threadid] = 0;
    _next_suspension_inst_count[threadid] = ROUND_ROBIN_INST_COUNT;
    _suspension_allowed[threadid] = true;
    _alive_threads[threadid] = true;
    printf("thread begin %d. _total_threads(%d)\n",threadid, _total_threads);
    fflush(stdout);
    PIN_ReleaseLock(&lock);
}

// This routine is executed every time a thread is destroyed.
VOID ThreadFini(THREADID threadid, const CONTEXT *ctxt, INT32 code, VOID *v)
{
   PIN_GetLock(&lock, threadid+1);
   _total_threads--;
   _alive_threads[threadid] = false;
   if(threadid == _currently_running_thread){
      // Time to yield control to some other task 
      unsigned int temp = _currently_running_thread;
      for(std::map<int, bool>::iterator it=_alive_threads.begin();
            it != _alive_threads.end(); it++)
      {
         temp = (temp+1) % _alive_threads.size();
         //if(temp == 0) temp++;
         if(_alive_threads[temp]){
            _currently_running_thread = temp;
            break;
         }
      }
   }

   // This would mean only one thread is left in the system
   // and Pthread_Join has also been called.
   if(_alive_threads[0]==false && _total_threads==1){
      _currently_running_thread = 0;
      _alive_threads[0] = true;
   }

   printf("thread end %d code %d, _total_threads(%d)\n",threadid, code, _total_threads);
   fflush(stdout);
   PIN_ReleaseLock(&lock);
}

// Enable/Disable statistics recording
void PinSimEnableModels()
{
   _enable_models = true;
   assert(_perf_model);
   _perf_model->EnableModels();
   _perf_model->RecordSimStart();
}

void PinSimDisableModels()
{
   _enable_models = false;
   assert(_perf_model);
   _perf_model->DisableModels();
   _perf_model->RecordSimEnd();
}

// Get simulator's time
uint64_t PinSimGetTime()
{
   assert(_perf_model);
   return _perf_model->SimGetTime();
}

void PinPthreadJoin(THREADID threadid)
{
   assert(threadid == 0);
   PIN_GetLock(&lock, threadid+1);
   //printf("Th(%d) called PinPthreadJoin.\n", threadid); fflush(stdout);
   
   _alive_threads[0] = false;
   unsigned int temp = _currently_running_thread;
   for(unsigned int i=0; i<_alive_threads.size(); i++)
   {
      temp = (temp+1) % _alive_threads.size();
      if(_alive_threads[temp]){
         _currently_running_thread = temp;
         break;
      }
   }
   assert(_currently_running_thread != 0);
   PIN_ReleaseLock(&lock);
}

// Recording Write Address in Register G0 
ADDRINT captureWriteEa(ADDRINT tgt_ea)
{
   return tgt_ea;
}

void BeforeSyscall(THREADID threadid, CONTEXT *ctxt, SYSCALL_STANDARD std, VOID *v)
{
   PIN_GetLock(&lock, threadid+1);
   _suspension_allowed[threadid] = false;
   //printf("Th(%d) Made syscall #%lu\n", threadid, 
     //    PIN_GetSyscallNumber(ctxt, std)); fflush(stdout);
   PIN_ReleaseLock(&lock);
}

void AfterSyscall(THREADID threadid, CONTEXT *ctxt, SYSCALL_STANDARD std, VOID *v)
{
   PIN_GetLock(&lock, threadid+1);
   _suspension_allowed[threadid] = true;
   //printf("Th(%d) Finished syscall.\n", threadid); fflush(stdout);
   PIN_ReleaseLock(&lock);
}

// Instruction Counter
void docount(THREADID threadid)
{
   if(_enable_models)
   {
      PIN_GetLock(&lock, threadid+1);
      assert(_perf_model);
      _perf_model->UpdateEventCounter(SimplePerfModel::INST);
      PIN_ReleaseLock(&lock);
   }
}

// Check for Context Switching
void CheckContextSwitch(THREADID threadid)
{
   if(_enable_models)
   {
      PIN_GetLock(&lock, threadid+1);
     
      // Check if it's time to context switch
      if( threadid == _currently_running_thread )
      {
         if( _inst_count_map[threadid] >= _next_suspension_inst_count[threadid] &&
               _suspension_allowed[threadid] ){

            // Time to yield control to some other task 
            // and suspend yourself on next instruction.
            _next_suspension_inst_count[threadid] += ROUND_ROBIN_INST_COUNT;
            
            unsigned int temp = _currently_running_thread;
            for(unsigned int i=0; i<_alive_threads.size(); i++)
            {
               temp = (temp+1) % _alive_threads.size();
               if(_alive_threads[temp]){
                  _currently_running_thread = temp;
                  break;
               }
            }
         }
      }

      // Suspension happen here.
      if(threadid != _currently_running_thread)
      {
         assert(_suspension_allowed[threadid]);
         printf("Thread(%d) suspending itself. Time(%lu)\n", threadid, _perf_model->SimGetTime());
         PIN_ReleaseLock(&lock);
         while(threadid != _currently_running_thread);
         PIN_GetLock(&lock, threadid+1);
         printf("Thread(%d) Resuming. Time(%lu)\n", threadid, _perf_model->SimGetTime());
      }

      _inst_count_map[threadid]++;
      PIN_ReleaseLock(&lock);
   }
}


// Memory Reference
void MemRef(THREADID threadid, ADDRINT address, int size, AccessType_t op_type)
{
   if(_enable_models)
   {
      // Check for context switching first.
      CheckContextSwitch(threadid);

      PIN_GetLock(&lock, threadid+1);

      assert(_perf_model);
      uint64_t start_addr_aligned = (address / CACHELINE_SIZE) * CACHELINE_SIZE;
      uint64_t end_addr_aligned = ((address+size-1) / CACHELINE_SIZE) * CACHELINE_SIZE;
      uint8_t data_buf[CACHELINE_SIZE];
      
      for(uint64_t addr=start_addr_aligned; addr<=end_addr_aligned; addr+=CACHELINE_SIZE )
      {
         // Lookup Physical Address
         uint64_t phy_addr = VirtualToPhysicalAddress(addr);

         // Access Cache
         if(op_type == ACCESS_TYPE_LOAD) // Read Accesses
         {
            //printf("R(%lu),Th(%d)  ",phy_addr, threadid); fflush(stdout);

            _perf_model->UpdateEventCounter(SimplePerfModel::CORE_READ);
            bool cache_hit = _cache_model->Read(phy_addr, data_buf);
           
            // Model Timing since read accesses are blocking
            if(cache_hit) {  // Cache Hit
               _perf_model->UpdateEventCounter(SimplePerfModel::CACHE_HIT);
            } 
            else {          // Cache Miss. Read drom DRAM
               _perf_model->UpdateEventCounter(SimplePerfModel::CACHE_MISS);

               // Injecting Faults for integrity verification
#if (DRAM_TYPE != 0)
               int divisor = (100*1.0)/FAULT_PROBABILITY; 
               if( FAULT_INJECTION && (rand() % divisor == 0) )
               {
                  printf("Injecting fault in cacheline %lu \n", phy_addr/CACHELINE_SIZE);
                  fflush(stdout);
                  _dram_model->InjectFault(phy_addr);
               }
#endif
               _dram_model->Read(phy_addr, data_buf);
               _perf_model->UpdateEventCounter(SimplePerfModel::DRAM_READ);
               
               // Store data in cache
               bool eviction = _cache_model->Write(phy_addr, data_buf);
               
               if(eviction){
                  uint64_t evicted_addr = _cache_model->evicted->tag;
                  _dram_model->Write(evicted_addr, data_buf);
                  _perf_model->UpdateEventCounter(SimplePerfModel::CACHE_EVICTION);
                  _perf_model->UpdateEventCounter(SimplePerfModel::DRAM_WRITE);
               }
            }
         }
         else if(op_type == ACCESS_TYPE_STORE)  // Write Accesses
         {
            //printf("W(%lu),Th(%d)  ",phy_addr, threadid); fflush(stdout);
            
            _perf_model->UpdateEventCounter(SimplePerfModel::CORE_WRITE);
            
            // Read the actual written data from App's address space
            memcpy(data_buf, (uint8_t*)addr, CACHELINE_SIZE);
            
            // Store data in cache
            bool eviction = _cache_model->Write(phy_addr, data_buf);
            
            // Write accesses are non-blocking and do not incur latency on critical path, 
            // so just count the dirty evictions and writeback the data to DRAM
            if(eviction){
               uint64_t evicted_addr = _cache_model->evicted->tag;
               _dram_model->Write(evicted_addr, data_buf);
               _perf_model->UpdateEventCounter(SimplePerfModel::CACHE_EVICTION);
               _perf_model->UpdateEventCounter(SimplePerfModel::DRAM_WRITE);
            }
         }
      }

      PIN_ReleaseLock(&lock);
   }
}


/**********************************************************************
 * 
 * Instrumentation Rotuines 
 *
 * ********************************************************************/

void Instruction(INS ins, void *v)
{
   /******************************
    * Instruction Counter rotuine
    * ***************************/
   //INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR) docount, IARG_THREAD_ID, IARG_END);
   INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR) docount, IARG_THREAD_ID, IARG_END);

   /******************************
    * Cache Modeling Routines
    * ***************************/
   // READS
   if( INS_IsMemoryRead(ins) )
   {
      INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR) MemRef, 
                     IARG_THREAD_ID, 
                     IARG_MEMORYREAD_EA,
                     IARG_MEMORYREAD_SIZE,
                     IARG_UINT32, ACCESS_TYPE_LOAD,
                     IARG_END );
   }

   //WRITES
   if (INS_IsMemoryWrite(ins))
   {
      INS_InsertCall(ins, IPOINT_BEFORE,
                     AFUNPTR(captureWriteEa),
                     IARG_MEMORYWRITE_EA,
                     IARG_RETURN_REGS, REG_INST_G0, // store IARG_MEMORYWRITE_EA in G0 
                     IARG_END);

      IPOINT ipoint = INS_HasFallThrough(ins) ? IPOINT_AFTER : IPOINT_TAKEN_BRANCH;
      INS_InsertCall(ins, ipoint, (AFUNPTR) MemRef,
                     IARG_THREAD_ID,
                     IARG_REG_VALUE, REG_INST_G0, // value of IARG_MEMORYWRITE_EA at IPOINT_BEFORE
                     IARG_MEMORYWRITE_SIZE,
                     IARG_UINT32, ACCESS_TYPE_STORE,
                     IARG_END);
   }
}

void RoutineCallback(RTN rtn, void* v)
{
   string rtn_name = RTN_Name(rtn);

   // Enable Models
   if (rtn_name == "SimEnableModels")
   {
      PROTO proto = PROTO_Allocate(PIN_PARG(void),
            CALLINGSTD_DEFAULT,
            "SimEnableModels",
            PIN_PARG_END());

      RTN_ReplaceSignature(rtn,
            AFUNPTR(PinSimEnableModels),
            IARG_PROTOTYPE, proto,
            IARG_END);

      PROTO_Free(proto);
   }
   
   // Disable Models
   if (rtn_name == "SimDisableModels")
   {
      PROTO proto = PROTO_Allocate(PIN_PARG(void),
            CALLINGSTD_DEFAULT,
            "SimDisableModels",
            PIN_PARG_END());

      RTN_ReplaceSignature(rtn,
            AFUNPTR(PinSimDisableModels),
            IARG_PROTOTYPE, proto,
            IARG_END);

      PROTO_Free(proto);
   }

   // Get Sim Time
   if (rtn_name == "SimGetTime")
   {
      PROTO proto = PROTO_Allocate(PIN_PARG(void),
            CALLINGSTD_DEFAULT,
            "SimGetTime",
            PIN_PARG_END());

      RTN_ReplaceSignature(rtn,
            AFUNPTR(PinSimGetTime),
            IARG_PROTOTYPE, proto,
            IARG_END);

      PROTO_Free(proto);
   }

   // Pthread Join
   if (rtn_name == "pthread_join")
   {
      RTN_Open(rtn);

      RTN_InsertCall(rtn, 
            IPOINT_BEFORE,
            (AFUNPTR) PinPthreadJoin,
            IARG_THREAD_ID,
            IARG_END); 

      RTN_Close(rtn);
   }
}


void Finish(INT32 code, void* v)
{
   // Print Summary
   _perf_model->PrintSummary();
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int main(int argc, char* argv[])
{
   srand (time(NULL));
   
   // Create Performance Model.
   _perf_model = new SimplePerfModel();
   assert(_perf_model);

   // Crate Cache Model
   _cache_model = new SimpleCache(CACHE_CAPACITY, CACHELINE_SIZE, CACHE_ASSOCIATIVITY);
   assert(_cache_model);

   // Create DRAM Model
#if (DRAM_TYPE == 1)  // Merkle Tree 
   _dram_model = new MerkleTreeDram(DRAM_CAPACITY, CACHELINE_SIZE, _perf_model);
#elif (DRAM_TYPE == 2) // MAC Tree
   _dram_model = new MacTreeDram(DRAM_CAPACITY, CACHELINE_SIZE, _perf_model);
#else
   _dram_model = new SimpleDram(DRAM_CAPACITY, CACHELINE_SIZE);
#endif
   assert(_dram_model);

   /**********************************************************************/
   
   // Initialize the pin lock
   PIN_InitLock(&lock);

   // Initialize PIN
   PIN_InitSymbols();
   PIN_Init(argc, argv);

   // Register Analysis routines to be called when a thread begins/ends
   PIN_AddThreadStartFunction(ThreadStart, 0);
   PIN_AddThreadFiniFunction(ThreadFini, 0);

   // Add Entry/Exit routines for System Calls
   PIN_AddSyscallEntryFunction(BeforeSyscall, 0);
   PIN_AddSyscallExitFunction(AfterSyscall, 0);

   // Register Routine callbacks
   RTN_AddInstrumentFunction(RoutineCallback, 0);

   // Register Instruction callbacks
   INS_AddInstrumentFunction(Instruction, 0);

   // Register "Finish" function which executes upon completion.
   PIN_AddFiniFunction(Finish, 0);
   
   // Start Application
   PIN_StartProgram();
   return 0;
}


/* ===================================================================== */
/* eof */
/* ===================================================================== */
