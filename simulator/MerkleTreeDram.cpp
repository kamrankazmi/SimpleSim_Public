#include "MerkleTreeDram.h"

MerkleTreeDram::MerkleTreeDram(int capacity, int blockSize, SimplePerfModel* perf_model)
{
   assert(perf_model);
   _perf_model = perf_model;
	
   Capacity = capacity * 1024 * 1024; // Mega Bytes to Bytes
	BlockSize = blockSize;
	Blocks = Capacity / BlockSize;

   // Tree scaling factor defines the size-ratio between two levels of the tree.
   _tree_scaling_factor = CACHELINE_SIZE/HASH_SIZE;

   // TODO: Compute space needed for Hash Tree and populate any data structures you need to construct the tree.
   // TODO: Specify total space needed (data blocks + hash tree blocks) in 'Total_Blocks'
   Total_Blocks += Blocks;    // Added space needed for data block. You need to add space needed for hash tree.
   printf("Constructing Merkle Tree: \n");

   
   
   
   //TODO: Add here 
   
   
   
   
   
   
   printf("Total Blocks = %lu\n", Total_Blocks);
   
   // Allocate Data DRAM Space
	DRAM = new MerkleTreeDramline [Total_Blocks];

   // Initialize
   for(uint64_t i=0; i<Total_Blocks; i++){
      DRAM[i].addr = i;
   }

   // Initialize Hash Tree
   InitializeTree();

	debug = 0;
}

MerkleTreeDram::~MerkleTreeDram() 
{ 
}

void MerkleTreeDram::InitializeTree()
{
   // TODO: Initialize Hash tree and Root node here.








   // After initialization, verify that the initialization was correct.
   for(uint64_t cl_num=0; cl_num<Blocks; cl_num++)
      VerifyHashChain(cl_num);
}

// Compute chain of addresses of hash-chain blocks for 'address'.
// Store address of i'th level block in Addr[i] starting from level 0.
void MerkleTreeDram::GenerateAddr(uint64_t address)
{
   assert(_levels < 20);
   assert(address < Blocks);  // This function only takes address of data level cache line, not hash tree node.

   Addr[0] = address;

   //TODO: Implement address computation for rest of the levels
}


// MerkleTreeDram::ComputeHash computes hash of given cache line buffer
// and stores the 16 Byte in buffer _hash.
void MerkleTreeDram::ComputeHash(uint8_t* data_buf)
{
   // Computes a 128 bit long hash of 64 byte cache line. 
   // Computed hash is stored in buffer _hash
   hashing_engine.CalculateDigest(_hash, (const uint8_t*)data_buf, CACHELINE_SIZE);
   
   // Performane Modeling. DO NOT CHANGE THIS PLEASE!
   _perf_model->UpdateEventCounter(SimplePerfModel::HASH_COMP);
   _perf_model->UpdateEventCounter(SimplePerfModel::DRAM_READ);
   if(_current_access_type == WRITE_ACCESS)
      _perf_model->UpdateEventCounter(SimplePerfModel::DRAM_WRITE);
}

// MerkleTreeDram::ComputeHashOfSiblings reads a cache line with address 'sibling'
// from DRAM, computes its hash and stores it at
// the corresponding place inside 'hash_buf' cache line.
void MerkleTreeDram::ComputeHashOfSiblings(uint64_t sibling, uint8_t* hash_buf)
{
   assert(hash_buf);

   // Read DRAM data
   MerkleTreeDramline* dram_line = &DRAM[sibling];
   assert(dram_line->data);
   assert(dram_line->addr == sibling);
   
   // Compute Hash
   ComputeHash(dram_line->data);
   
   // Write in buffer at corresponding offset
   uint64_t sibling_index = (sibling % _tree_scaling_factor);
   memcpy(hash_buf+(sibling_index*HASH_SIZE), _hash, HASH_SIZE);
}

// MerkleTreeDram::CompareHashes compares two hashes provided 
// in hash_buf1 and hash_buf2, and returns True if they match,
// otherwise returns false.
bool MerkleTreeDram::CompareHashes(uint8_t* hash_buf1, uint8_t* hash_buf2)
{
   assert(hash_buf1);
   assert(hash_buf2);
   for(int i=0; i<CACHELINE_SIZE; i++)
   {
      if(hash_buf1[i] != hash_buf2[i])
         return false;
   }
   return true;
}

// MerkleTreeDram::CompareWithRootHash compares the given hash_buf
// with root hash, and returns True if they match,
// otherwise returns false.
bool MerkleTreeDram::CompareWithRootHash(uint8_t* hash_buf)
{
   assert(hash_buf);
   for(int i=0; i<CACHELINE_SIZE; i++)
   {
      if(hash_buf[i] != Root_Hash.data[i] )
         return false;
   }
   return true;
}

// MerkleTreeDram::VerifyHashChain verifies the hash chain 
// for a given data block address 'data_cl_num'
// from leaf node up to the root node.
// It crashes the application immediately if verification fails.
// It returns true upon successful verification.
bool MerkleTreeDram::VerifyHashChain(uint64_t data_cl_num)
{
   assert(data_cl_num < Blocks);
   GenerateAddr(data_cl_num);    // Generating address chain.

   // TODO: Implement hash chain verification.
   // TODO: Call 'assert(false)' function if verification fails to crash the application.
   
   
  

   // If reached here, verification successful.
   return true;
}

// MerkleTreeDram::UpdateHashChain updates the hash chain 
// for a given data block address 'data_cl_num'
// from leaf node up to the root node.
void MerkleTreeDram::UpdateHashChain(uint64_t data_cl_num)
{
   assert(data_cl_num < Blocks);
   GenerateAddr(data_cl_num);

   // TODO: Implement updating the hash chain
   

}


// MerkleTreeDram::Read is called by the simulator_main.cc
// upon read requests.
void MerkleTreeDram::Read(uint64_t addr, uint8_t* data_buf)
{
   _current_access_type = READ_ACCESS; 
   uint64_t cl_num = addr / CACHELINE_SIZE;
   assert(cl_num < Blocks);

   MerkleTreeDramline* dram_line = &DRAM[cl_num];
   assert(dram_line->data && data_buf);
   assert(dram_line->addr == cl_num);

   // Verify Hash Chain
   VerifyHashChain(cl_num);

   // Read data from DRAM model
   memcpy(data_buf, dram_line->data, CACHELINE_SIZE);
}


// MerkleTreeDram::Write is called by the simulator_main.cc
// upon write requests.
void MerkleTreeDram::Write(uint64_t addr, uint8_t* data_buf)
{	
   _current_access_type = WRITE_ACCESS; 
   uint64_t cl_num = addr / CACHELINE_SIZE;
   assert(cl_num < Blocks);

   MerkleTreeDramline* dram_line = &DRAM[cl_num];
   assert(dram_line->data && data_buf);
   assert(dram_line->addr == cl_num);

   // Update data in DRAM model
   memcpy(dram_line->data, data_buf, CACHELINE_SIZE);

   //Update Hash Chain
   UpdateHashChain(cl_num);
}


// MerkleTreeDram::InjectFault injets a fault on-purpose at a 
// random location in the cache line to test the integrity verification scheme's effectiveness.
// After the fault injection, upon subsequent read to this cache line
// the system should detect a 'malicious modification' of this data.
// Falut injector is called by simulator_main.cc and can be enabled/disabled in parameters.h
void MerkleTreeDram::InjectFault(uint64_t addr)
{
   uint64_t cl_num = addr / CACHELINE_SIZE;
   assert(cl_num < Blocks);

   MerkleTreeDramline* dram_line = &DRAM[cl_num];
   assert(dram_line->data);
   assert(dram_line->addr == cl_num);

   // Inject some fault
   memset(dram_line->data+(rand()%CACHELINE_SIZE), rand()%256, 1);
}

