#ifndef SIMPLE_DRAM_H
#define SIMPLE_DRAM_H

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h>

#include <stdint.h>
#include <cstdio>
#include <assert.h>
#include <string.h>
#include <map>

#include "Parameters.h"

class SimpleDramline
{
public:
	SimpleDramline(uint64_t addr = 0, uint8_t* data=NULL)
	{
		this->addr = addr;
      if(data != NULL)
         memcpy(this->data, data, CACHELINE_SIZE);
      else
         memset(this->data, 0x00, CACHELINE_SIZE);
	}
	
	uint64_t addr;
   uint8_t data[CACHELINE_SIZE];
};

class SimpleDram
{
public:
	SimpleDram(int capacity, int blockSize);
	~SimpleDram();
	void Read(uint64_t addr, uint8_t* data_buf);
	void Write(uint64_t addr, uint8_t* data_buf);
	
private:
	uint64_t Capacity;	// in Mega Bytes
	uint64_t BlockSize;
	uint64_t Blocks;

   /*
   std::map<uint64_t, SimpleDramline*> DRAM;
   std::map<uint64_t, SimpleDramline*>::iterator iter;
	*/

   SimpleDramline* DRAM;

	short debug;
};

#endif
