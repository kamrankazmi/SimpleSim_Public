#ifndef SIM_USER_H
#define SIM_USER_H

#ifdef __cplusplus
extern "C"
{
#endif

// These functions' signatures are replaced by the simulator's function, 
// hence the definitions written here are never executed.
void __attribute__((optimize("O0"))) SimEnableModels(){}             // Enable simulator models
void __attribute__((optimize("O0"))) SimDisableModels(){}            // Disable Simulator Models
unsigned long long int __attribute__((optimize("O0"))) SimGetTime() { return 0; }  // Get simulator's current time.

#ifdef __cplusplus
}
#endif

#endif // CARBON_USER_H
