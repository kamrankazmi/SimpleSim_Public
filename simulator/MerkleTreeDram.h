#ifndef MERKLE_TREE_DRAM_H
#define MERKLE_TREE_DRAM_H

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h>

#include <stdint.h>
#include <cstdio>
#include <assert.h>
#include <string.h>
#include <map>

#include "Parameters.h"
#include "Perf_Model.h"
#include "SimplePerfModel.h"

class MerkleTreeDramline
{
public:
	MerkleTreeDramline(uint64_t addr = 0, uint8_t* data=NULL)
	{
		this->addr = addr;
      if(data != NULL)
         memcpy(this->data, data, CACHELINE_SIZE);
      else
         memset(this->data, 0x00, CACHELINE_SIZE);
	}
	uint64_t addr;
   uint8_t data[CACHELINE_SIZE];
};

class MerkleTreeDram
{
public:
   enum AccessType{
      READ_ACCESS = 0,
      WRITE_ACCESS
   };

	MerkleTreeDram(int capacity, int blockSize, SimplePerfModel* perf_model);
	~MerkleTreeDram();
	void Read(uint64_t addr, uint8_t* data_buf);
	void Write(uint64_t addr, uint8_t* data_buf);
   void InjectFault(uint64_t addr);

private:
   void InitializeTree();
   bool VerifyHashChain(uint64_t data_cl_num);
   void UpdateHashChain(uint64_t data_cl_num);

   // Utilities
   void GenerateAddr(uint64_t address);
   void ComputeHash(uint8_t* data_buf);
   void ComputeHashOfSiblings(uint64_t sibling, uint8_t* hash_buf);
   bool CompareHashes(uint8_t* hash_buf1, uint8_t* hash_buf2);
   bool CompareWithRootHash(uint8_t* hash_buf);

   // Performance Model for timing modeling
   SimplePerfModel* _perf_model;

   uint64_t Capacity;	// in Mega Bytes
	uint64_t BlockSize;  // Cache line size
	uint64_t Blocks;     // Logical Blocks for user data.

   MerkleTreeDramline* DRAM;

   // Integrity Verfication related
   AccessType _current_access_type;
   CryptoPP::Weak::MD5 hashing_engine;
   uint8_t _hash[HASH_SIZE];
   MerkleTreeDramline Root_Hash;
   
   uint8_t _tree_scaling_factor;
   uint64_t _valid_blocks_count[20];
   uint64_t _address_offset[20];
   uint64_t _levels;
   uint64_t Total_Blocks;
   uint64_t Addr[20];
   
   short debug;
};

#endif
