#ifndef MAC_TREE_DRAM_H
#define MAC_TREE_DRAM_H

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h>

#include <stdint.h>
#include <cstdio>
#include <assert.h>
#include <string.h>
#include <map>

#include "Parameters.h"
#include "Perf_Model.h"
#include "SimplePerfModel.h"

class MacTreeDramline
{
public:
	MacTreeDramline(uint64_t addr = 0, uint8_t* data=NULL)
	{
		this->addr = addr;
      if(data != NULL)
         memcpy(this->data, data, CACHELINE_SIZE);
      else
         memset(this->data, 0x00, CACHELINE_SIZE);
	}
	uint64_t addr;
   uint8_t data[CACHELINE_SIZE];
};

class MacTreeDram
{
public:
   enum AccessType{
      READ_ACCESS = 0,
      WRITE_ACCESS
   };

	MacTreeDram(int capacity, int blockSize, SimplePerfModel* perf_model);
	~MacTreeDram();
	void Read(uint64_t addr, uint8_t* data_buf);
	void Write(uint64_t addr, uint8_t* data_buf);
   void InjectFault(uint64_t addr);

private:
   void InitializeTree();
   bool VerifyHashChain(uint64_t data_cl_num);
   void UpdateHashChain(uint64_t data_cl_num);

   // Utilities
   void PrintCacheLine(uint8_t* data_buf);
   void PrintTag(uint8_t* tag_buf);
   void GenerateAddr(uint64_t address);
   void ComputeHash(uint8_t* data_buf);
   void ComputeMac(uint8_t* data_buf, uint64_t counter);
   void ComputeHashOfSiblings(uint64_t sibling, uint8_t* hash_buf);
   bool CompareHashes(uint8_t* hash_buf1, uint8_t* hash_buf2);
   bool CompareWithRootHash(uint8_t* hash_buf);
   bool CompareTags(uint8_t* tag_buf1, uint8_t* tag_buf2);
   uint64_t getDataTagAddr(uint64_t cl_num);
   uint64_t getDataTag(uint64_t cl_num);
   void setDataTag(uint64_t cl_num, uint8_t* tag);
   uint64_t getTag(uint8_t* data_buf);
   uint64_t getCounter(uint8_t* data_buf, int ctr_index);
   uint64_t getCounterIndex(uint64_t cl_num);
   void setTag(uint8_t* data_buf, uint8_t* tag);
   void setCounter(uint8_t* data_buf, int ctr_index, uint64_t ctr_value);


   // Performance Model for timing modeling
   SimplePerfModel* _perf_model;

   uint64_t Capacity;	// in Mega Bytes
	uint64_t BlockSize;  // Cache line size
	uint64_t Blocks;     // Logical Blocks for user data.

   MacTreeDramline* DRAM;

   // Integrity Verfication related
   AccessType _current_access_type;
   CryptoPP::Weak::MD5 hashing_engine;
   uint8_t _hash[HASH_SIZE];
   uint8_t _tag[TAG_SIZE];
   MacTreeDramline Root_Node;
   MacTreeDramline Root_Hash;
   
	uint64_t _data_tag_blocks;       // Blocks reserved for tags of user data cache lines.
   uint8_t  _tree_scaling_factor;
   uint64_t _valid_blocks_count[20];
   uint64_t _address_offset[20];
   uint64_t _levels;
   uint64_t Total_Blocks;
   uint64_t Addr[20];
   
   short debug;
};

#endif
