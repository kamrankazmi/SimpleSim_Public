#ifndef PARAMETERS_H
#define PARAMETERS_H

/* ================================================================== */
// Global Parameters 
/* ================================================================== */
#define CACHELINE_SIZE        64
#define CACHE_CAPACITY        65536    // in Bytes (64 kB)
#define CACHE_ASSOCIATIVITY   4        // Ways in the cache
#define CACHE_HIT_COST        5       // in Nano Seconds   
#define CACHE_MISS_COST       50      // in Nano Seconds

#define PAGE_SIZE             4096     // Bytes

#define DRAM_CAPACITY         1        // User visible capacity in Mega Bytes
#define DRAM_TYPE             0        // 0: Simple Dram
                                       // 1: Merkle Tree Integrity Verification
                                       // 2: MAC Tree Integrity Verification
// Merkle Tree related
#define HASH_SIZE             CryptoPP::Weak::MD5::DIGESTSIZE  // 16 Bytes or 128 bits.
#define HASH_COMP_COST        10       // in Nano Seconds

// Mac Tree Related
#define TAG_SIZE              8        // 8 Bytes or 64 bits.
#define COUNTER_SIZE          7        // 7 Bytes or 56 bits.
#define TAG_COMP_COST         10       // in Nano Seconds.
#define COUNTER_BITS          56       // in bits
#define TAG_BITS              64       // in bits

#define FAULT_INJECTION       0        // Enable/Diable Fault injection to test integrity verification. 1=Enable, 0=Disable
#define FAULT_PROBABILITY     0.1      // in percentage


#define ROUND_ROBIN_INST_COUNT   10000  // # of Memory Instructions after which a context switch happens.
/* ================================================================== */

#endif
