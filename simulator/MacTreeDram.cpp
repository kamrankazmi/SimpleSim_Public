#include "MacTreeDram.h"

MacTreeDram::MacTreeDram(int capacity, int blockSize, SimplePerfModel* perf_model)
{
   assert(perf_model);
   _perf_model = perf_model;
	
   Capacity = capacity * 1024 * 1024; // Mega Bytes to Bytes
	BlockSize = blockSize;
	Blocks = Capacity / BlockSize;
   _data_tag_blocks = (Blocks / (CACHELINE_SIZE/TAG_SIZE) );

   // Tree scaling factor defines the size-ratio between two levels of the tree.
   _tree_scaling_factor = (CACHELINE_SIZE*8 - TAG_BITS) / COUNTER_BITS;
   
   // TODO: Compute space needed for Mac Tree and populate any data structures you need to construct the tree.
   // TODO: Specify total space needed (data blocks + Mac tree blocks) in 'Total_Blocks'
   Total_Blocks += Blocks;    // Added space needed for data block. You need to add space needed for hash tree.
   printf("Constructing Mac Tree: \n");
   
   
   
   
   //TODO: Add here 
   
   
   
   
   
   printf("Total Blocks = %lu\n", Total_Blocks);
   
   // Allocate Data DRAM Space
	DRAM = new MacTreeDramline [Total_Blocks];

   // Initialize
   for(uint64_t i=0; i<Total_Blocks; i++){
      DRAM[i].addr = i;
   }

   // Initialize Hash Tree
   InitializeTree();

	debug = 0;
}

MacTreeDram::~MacTreeDram() 
{ 
}

void MacTreeDram::InitializeTree()
{
   // TODO: Initialize Mac tree and Root node here.
  







   // After initialization, verify that the initialization was correct.
   for(uint64_t cl_num=0; cl_num<Blocks; cl_num++){
      VerifyHashChain(cl_num);
   }
}

// ************************************************************************
//
// Several useful functions that I used.
// You may or may not want to use them.
//
// ************************************************************************

uint64_t MacTreeDram::getDataTagAddr(uint64_t cl_num)
{
   assert(cl_num < Blocks);
   uint64_t tag_addr = Blocks + (cl_num / (CACHELINE_SIZE/TAG_SIZE) );
   return tag_addr;
}

uint64_t MacTreeDram::getDataTag(uint64_t cl_num)
{
   assert(cl_num < Blocks);
   uint64_t tag_addr = getDataTagAddr(cl_num);
   uint8_t offset = (cl_num % (CACHELINE_SIZE/TAG_SIZE)) * TAG_SIZE;
   uint64_t tag;
   memcpy((uint8_t*)&tag, DRAM[tag_addr].data + offset, TAG_SIZE);
   return tag;
}

void MacTreeDram::setDataTag(uint64_t cl_num, uint8_t* tag)
{
   assert(cl_num < Blocks);
   assert(tag);
   uint64_t tag_addr = getDataTagAddr(cl_num);
   uint8_t offset = (cl_num % (CACHELINE_SIZE/TAG_SIZE)) * TAG_SIZE;
   memcpy(DRAM[tag_addr].data + offset, tag, TAG_SIZE);
}

uint64_t MacTreeDram::getTag(uint8_t* data_buf)
{
   assert(data_buf);
   uint64_t tag;
   memcpy((uint8_t*)&tag, data_buf, TAG_SIZE);
   return tag;
}

uint64_t MacTreeDram::getCounter(uint8_t* data_buf, int ctr_index)
{
   assert(data_buf);
   uint64_t counter;
   
   // Skip first 64 bits which are reserved for tag.
   int byte_index = TAG_SIZE + (ctr_index * COUNTER_SIZE);
   
   memcpy((uint8_t*)&counter, data_buf+byte_index, COUNTER_SIZE);
   return counter;
}

uint64_t MacTreeDram::getCounterIndex(uint64_t cl_num)
{
   uint8_t level = 0;
   while(_address_offset[level+1] < cl_num)
      level++;

   assert(level < _levels);
   uint64_t ctr_index = (cl_num - _address_offset[level]) % (CACHELINE_SIZE/TAG_SIZE);
   return ctr_index;
}

void MacTreeDram::setTag(uint8_t* data_buf, uint8_t* tag)
{
   assert(data_buf && tag);
   memcpy(data_buf, tag, TAG_SIZE);
}

void MacTreeDram::setCounter(uint8_t* data_buf, int ctr_index, uint64_t ctr_value)
{
   assert(data_buf);
   int byte_index = TAG_SIZE + (ctr_index * COUNTER_SIZE);
   memcpy(data_buf+byte_index, (uint8_t*)&ctr_value, COUNTER_SIZE);
}

void MacTreeDram::PrintCacheLine(uint8_t* data_buf)
{
   assert(data_buf);
   for(int i=0; i<CACHELINE_SIZE; i++)
      printf("%x ", data_buf[i]);
   printf("\n");
}

void MacTreeDram::PrintTag(uint8_t* tag_buf)
{
   assert(tag_buf);
   for(int i=0; i<TAG_SIZE; i++)
      printf("%x ", tag_buf[i]);
   printf("\n");
}

// ************************************************************************


// Compute chain of addresses of mac-chain blocks for 'address'.
// Store address of i'th level block in Addr[i] starting from level 0.
void MacTreeDram::GenerateAddr(uint64_t address)
{
   assert(_levels < 20);
   assert(address < Blocks);  // This function only takes address of data level cache line, not hash tree node.
   
   Addr[0] = address;
   
   //TODO: Implement address computation for rest of the levels
}


// MacTreeDram::ComputeHash computes hash of given cache line buffer
// and stores the 16 Byte in buffer _hash.
void MacTreeDram::ComputeHash(uint8_t* data_buf)
{
   // Compute a 128 bit long hash of 64 byte cache line. 
   // Computed hash is stored in buffer _hash
   hashing_engine.CalculateDigest(_hash, (const uint8_t*)data_buf, CACHELINE_SIZE);
   
   // Performane Modeling
   _perf_model->UpdateEventCounter(SimplePerfModel::HASH_COMP);
   _perf_model->UpdateEventCounter(SimplePerfModel::DRAM_READ);
   if(_current_access_type == WRITE_ACCESS)
      _perf_model->UpdateEventCounter(SimplePerfModel::DRAM_WRITE);
}
   
// MacTreeDram::ComputeMac computes mac of given cache line buffer
// using the provided counter
// and stores the 8 Byte MAC in buffer _tag.
void MacTreeDram::ComputeMac(uint8_t* data_buf, uint64_t counter)
{
   // Mac is computed as follows: 
   // _tag = Hash( Hash(Data) || Counter )
   // Only first 8 bytes of _hash are kept in _tag.

   MacTreeDramline* node = new MacTreeDramline;
   hashing_engine.CalculateDigest(_hash, (const uint8_t*)data_buf, CACHELINE_SIZE);
   memcpy(node->data, _hash, HASH_SIZE);
   memcpy(node->data+HASH_SIZE, (const uint8_t*)&counter, sizeof(uint64_t));
   hashing_engine.CalculateDigest(_hash, (const uint8_t*)node->data, CACHELINE_SIZE);
   memcpy(_tag, _hash, TAG_SIZE);

   // Performane Modeling
   _perf_model->UpdateEventCounter(SimplePerfModel::HASH_COMP);
   _perf_model->UpdateEventCounter(SimplePerfModel::DRAM_READ);
   if(_current_access_type == WRITE_ACCESS)
      _perf_model->UpdateEventCounter(SimplePerfModel::DRAM_WRITE);
}

// MacTreeDram::ComputeHashOfSiblings reads a cache line with address 'sibling'
// from DRAM, computes its hash and stores it at
// the corresponding place inside 'hash_buf' cache line.
void MacTreeDram::ComputeHashOfSiblings(uint64_t sibling, uint8_t* hash_buf)
{
   assert(hash_buf);

   // Read DRAM data
   MacTreeDramline* dram_line = &DRAM[sibling];
   assert(dram_line->data);
   assert(dram_line->addr == sibling);
   
   // Compute Hash
   ComputeHash(dram_line->data);
   
   // Write in buffer at corresponding offset
   uint64_t sibling_index = (sibling % _tree_scaling_factor);
   memcpy(hash_buf+(sibling_index*HASH_SIZE), _hash, HASH_SIZE);
}


// MacTreeDram::CompareHashes compares two hashes provided 
// in hash_buf1 and hash_buf2, and returns True if they match,
bool MacTreeDram::CompareHashes(uint8_t* hash_buf1, uint8_t* hash_buf2)
{
   assert(hash_buf1);
   assert(hash_buf2);
   for(int i=0; i<CACHELINE_SIZE; i++)
   {
      if(hash_buf1[i] != hash_buf2[i])
         return false;
   }
   return true;
}

// MacTreeDram::CompareWithRootHash compares the given hash_buf
// with root hash, and returns True if they match,
// otherwise returns false.
bool MacTreeDram::CompareWithRootHash(uint8_t* hash_buf)
{
   assert(hash_buf);
   for(int i=0; i<CACHELINE_SIZE; i++)
   {
      if(hash_buf[i] != Root_Hash.data[i] )
         return false;
   }
   return true;
}

// MacTreeDram::CompareTags compares two tags provided 
// in tag_buf1 and tag_buf2, and returns True if they match,
bool MacTreeDram::CompareTags(uint8_t* tag_buf1, uint8_t* tag_buf2)
{
   assert(tag_buf1);
   assert(tag_buf2);
   for(int i=0; i<TAG_SIZE; i++)
   {
      if(tag_buf1[i] != tag_buf2[i])
         return false;
   }
   return true;
}

// MacTreeDram::VerifyHashChain verifies the mac chain 
// for a given data block address 'data_cl_num'
// from leaf node up to the root node.
// It crashes the application immediately if verification fails.
// It returns true upon successful verification.
bool MacTreeDram::VerifyHashChain(uint64_t data_cl_num)
{
   assert(data_cl_num < Blocks);
   GenerateAddr(data_cl_num);    // Generating address chain.

   // TODO: Implement hash chain verification.
   // TODO: Call 'assert(false)' function if verification fails to crash the application.






   // If reached here, verification successful.
   return true;
}

// MacTreeDram::UpdateHashChain updates the mac chain 
// for a given data block address 'data_cl_num'
// from leaf node up to the root node.
void MacTreeDram::UpdateHashChain(uint64_t data_cl_num)
{
   assert(data_cl_num < Blocks);
   GenerateAddr(data_cl_num);

   // TODO: Implement updating the hash chain


}

// MacTreeDram::Read is called by the simulator_main.cc
// upon read requests.
void MacTreeDram::Read(uint64_t addr, uint8_t* data_buf)
{
   _current_access_type = READ_ACCESS; 
   uint64_t cl_num = addr / CACHELINE_SIZE;
   assert(cl_num < Blocks);
   
   MacTreeDramline* dram_line = &DRAM[cl_num];
   assert(dram_line->data && data_buf);
   assert(dram_line->addr == cl_num);

   // Verify Hash Chain
   VerifyHashChain(cl_num);

   // Read data from DRAM model
   memcpy(data_buf, dram_line->data, CACHELINE_SIZE);
}


// MacTreeDram::Write is called by the simulator_main.cc
// upon write requests.
void MacTreeDram::Write(uint64_t addr, uint8_t* data_buf)
{	
   _current_access_type = WRITE_ACCESS; 
   uint64_t cl_num = addr / CACHELINE_SIZE;
   assert(cl_num < Blocks);
   
   MacTreeDramline* dram_line = &DRAM[cl_num];
   assert(dram_line->data && data_buf);
   assert(dram_line->addr == cl_num);

   // Update data in DRAM model
   memcpy(dram_line->data, data_buf, CACHELINE_SIZE);

   //Update Hash Chain
   UpdateHashChain(cl_num);
}

// MacTreeDram::InjectFault injets a fault on-purpose at a 
// random location in the cache line to test the integrity verification scheme's effectiveness.
// After the fault injection, upon subsequent read to this cache line
// the system should detect a 'malicious modification' of this data.
// Falut injector is called by simulator_main.cc and can be enabled/disabled in parameters.h
void MacTreeDram::InjectFault(uint64_t addr)
{
   uint64_t cl_num = addr / CACHELINE_SIZE;
   assert(cl_num < Blocks);

   MacTreeDramline* dram_line = &DRAM[cl_num];
   assert(dram_line->data);
   assert(dram_line->addr == cl_num);

   // Inject some fault
   memset(dram_line->data+(rand()%CACHELINE_SIZE), rand()%256, 1);
}

