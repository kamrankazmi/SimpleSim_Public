#ifndef SIMPLE_CACHE_H
#define SIMPLE_CACHE_H

#include <stdint.h>
#include <cstdio>
#include <assert.h>
#include <string.h>

#include "Parameters.h"

class SimpleCacheline
{
public:
	SimpleCacheline(int valid = 0, uint64_t tag = 0, uint64_t last_time = 0, int dirty = 0)
	{
		this->valid = valid;
      this->dirty = dirty;
		this->tag = tag;
		this->last_time = last_time;
      memset(this->data, 0x00, CACHELINE_SIZE);
	}
	
	bool valid;
	uint64_t tag;
	uint64_t last_time;
   bool dirty;
   uint8_t data[CACHELINE_SIZE];
};

class SimpleCache
{
public:
	SimpleCache(int capacity, int blockSize, int ways);
	~SimpleCache();
	bool Read(uint64_t addr, uint8_t* data_buf);       // Returns true if Cache hit; false if Cache miss.

	bool Write(uint64_t addr, uint8_t* data_buf);      // Returns:
                                                      // - True if eviction happens. Evicted data is stored at "evicted->data". Evicted Address stored at "evicted->tag"
                                                      // - False if no eviction happens.
	
   SimpleCacheline * evicted;                         // Variable to temporarily hold evicted cacheline. 

private:
	int Capacity;	   // in Bytes, power of 2
	int BlockSize;    // Cacheline size in Bytes
	int Ways;         // Associativity
	int Blocks;       // Capacity in terms of Total Cachelines
	int Sets;	      // Total Cache sets

	SimpleCacheline * cache;
	
   uint64_t numAccess;	// Internal counter used as Time-stamp for LRU policy.
};

#endif
