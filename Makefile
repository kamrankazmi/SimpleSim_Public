SIM_ROOT ?= $(CURDIR)

CLEAN=$(findstring clean,$(MAKECMDGOALS))

PIN_SIM_LIB = $(SIM_ROOT)/lib/pin_sim.so

all: $(PIN_SIM_LIB)

include tests/benchmarks/Makefile

.PHONY: $(PIN_SIM_LIB)
$(PIN_SIM_LIB):
	$(MAKE) -C $(SIM_ROOT)/simulator

clean:
	$(MAKE) -C simulator clean
	$(MAKE) -C tests/benchmarks clean

#clean_output_dirs:
#	rm -f $(SIM_ROOT)/results/latest
#	rm -rf $(SIM_ROOT)/results/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9]-[0-9][0-9]-[0-9][0-9]

