This repository contains a simple Pin based simulator to model
a simple set associative cache and DRAM with memory integrity verification.


******* Compiling ********
Simply run "make" in the root direcotry of the simulator:

$ cd SimpleSim
$ make


******* Running the test application ********
Currently, a simple application "counters" is added under 
tests/benchmarks/counters/counters.cc

To run this application, do
$ make counters_bench_test




******* Adding your own applications *********
1. Copy the "counters" folder in tests/benchmarks/ and rename it with 
your application's name, e.g. <your_app>. 

2. Edit the <your_app>/Makefile to do the following:
    a) List all your source files under "SOURES"
    b) Edit "TARGET" to name your app, e.g. TARGET = <your_app>
    c) List any argments to your application under "APP_FLAGS"
    
3. To compile and run your app, do
$ make <your_app>_bench_test
