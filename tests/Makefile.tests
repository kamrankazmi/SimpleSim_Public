# This Makefile is included by individual tests.
#
# README: Variables that you might care about. Most of these can be
# overridden, which eliminates the variables they are derived from as
# being important. Unfortunately, I don't know how to break make
# functions across multiple lines, so run_fn is completely illegible.
#
# SOURCES - source files to build
# TARGET - name of executable
# RUN - command to run after building; derived from BUILD_MODE, MODE, EXEC, PROCS, and SIM_FLAGS
#
# CORES - number of cores
# APP_FLAGS - flags to pass to the application

SIM_ROOT ?= $(CURDIR)/../../..

# Enable/Disable Debugging. 
# sim ==> for debugging the simulator
# app ==> for debuggin app
# none ==> for no debugging
DEBUG = none



ifeq ($(DEBUG),sim)
   PIN_DEBUG_FLAGS = -pause_tool 20
   DBG_FLAGS = -g
   OPT_FLAGS = -O0 
else
ifeq ($(DEBUG),app)
   PIN_DEBUG_FLAGS = -appdebug
   DBG_FLAGS = -g
   OPT_FLAGS = -O0 
endif
endif



# Run options
include $(SIM_ROOT)/Makefile.config

# Which output dir to put the results in?
# The default output dir corresponds to the date and time when the simulation was started
#OUTPUT_DIR ?= $(shell date +%Y-%m-%d_%H-%M-%S)

PIN_BIN = $(PIN_HOME)/intel64/bin/pinbin
PIN_TOOL = $(SIM_ROOT)/lib/pin_sim
PIN_RUN = $(PIN_BIN) $(PIN_DEBUG_FLAGS) -injection child -tool_exit_timeout 1 -mt -t $(PIN_TOOL)

#OUTPUT_DIR_ABS_PATH := $(SIM_ROOT)/results/$(OUTPUT_DIR)

EXEC ?= $(CURDIR)/$(TARGET) $(APP_FLAGS) # Application command thats actually executed
run_fn = $(PIN_RUN) -- $(EXEC)
RUN ?= cd $(SIM_ROOT) ; $(call run_fn)


# Build targets
all: $(TARGET)
	$(RUN)

# Math, pthread libraries
LD_LIBS += $(APP_SPECIFIC_LD_LIBS)
LD_LIBS += -lm -pthread -lcryptopp

OBJECTS ?= $(patsubst %.cpp,%.o,$(patsubst %.c,%.o,$(patsubst %.cc,%.o,$(SOURCES) ) ) )

CLEAN=$(findstring clean,$(MAKECMDGOALS))

CXXFLAGS = $(DBG_FLAGS) $(OPT_FLAGS) -Wall $(APP_SPECIFIC_CXX_FLAGS) -I$(SIM_ROOT) -I$(SIM_ROOT)/simulator
CFLAGS = $(CXXFLAGS) #-std=c99

# Rules
PIN_SIM_LIB = $(SIM_ROOT)/lib/pin_sim.so

.PHONY: $(TARGET)
$(TARGET): $(OBJECTS)
	make -C $(SIM_ROOT)/simulator
	if $(foreach source,$(OBJECTS),[ ! -e $(TARGET) ] || [ $(source) -nt $(TARGET) ] ||) [ $(PIN_SIM_LIB) -nt $(TARGET) ]; \
	then $(CXX) $(OPT_FLAGS) $(DBG_FLAGS) $^ -o $@ $(LD_FLAGS) $(LD_LIBS); \
	fi


ifeq ($(CLEAN),)
-include $(OBJECTS:%.o=%.d)
endif

ifneq ($(CLEAN),)
clean:
	$(RM) *.o *.d $(TARGET)
endif

# Build rules for dependency generation
%.d: %.cpp
	$(CXX) -MM -MT $*.o -MF $@ $(CXXFLAGS) $<

%.d: %.cc
	$(CXX) -MM -MT $*.o -MF $@ $(CXXFLAGS) $<

%.d: %.c
	$(CC) -MM -MT $*.o -MF $@ $(CFLAGS) $<

# Build rules for object generation
%.o : %.cpp
	   $(CXX) -c $(CXXFLAGS) -o $@ $<
		   
%.o : %.cc
	   $(CXX) -c $(CXXFLAGS) -o $@ $<
		   
%.o : %.c
	   $(CC) -c $(CFLAGS) -o $@ $<

