#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Parameters.h"
#include "SimUser.h"

using namespace std;

#define DEFAULT_MAX_COUNT     100000
#define NUM_COUNTERS          100000

int max_count;
int counter[NUM_COUNTERS];// __attribute__ ((aligned (CACHELINE_SIZE)));

int main(int argc, char *argv[])
{
   if(argc > 1)
      max_count = atoi(argv[1]);
   else
      max_count = DEFAULT_MAX_COUNT;

   printf("Starting Application... \n");

   SimEnableModels();
   while(max_count > 0)
   {
      counter[ max_count % NUM_COUNTERS ] ++;
      max_count--;
   }
   SimDisableModels();
   printf("Done...! \n");
   return 0;
}

