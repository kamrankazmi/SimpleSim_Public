#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "Parameters.h"
#include "SimUser.h"

using namespace std;

#define DEFAULT_MAX_COUNT     100000
#define NUM_COUNTERS          100000

int max_count;
int counter[NUM_COUNTERS];// __attribute__ ((aligned (CACHELINE_SIZE)));

void* victim_task(void* arg)
{
   printf("Victim Task Started.\n");
   
   int local_max_count = max_count;
   while(local_max_count > 0)
   {
      counter[ local_max_count % NUM_COUNTERS ] ++;
      local_max_count--;
   }
   
   printf("Victim: Time(%lu)\n", SimGetTime());

   return 0;
}

void* attacker_task(void* arg)
{
   printf("Attacker Task Started.\n");
   
   int local_max_count = max_count;
   while(local_max_count > 0)
   {
      counter[ local_max_count % NUM_COUNTERS ] ++;
      local_max_count--;
   }

   printf("Attacker: Time(%lu)\n", SimGetTime());

   return 0;
}

int main(int argc, char *argv[])
{
   if(argc > 1)
      max_count = atoi(argv[1]);
   else
      max_count = DEFAULT_MAX_COUNT;

   pthread_t attacker_handle;
   int ret_a;

   printf("Starting Application... \n");
   SimEnableModels();
  
   // Spawn attacker thread
   ret_a = pthread_create(&attacker_handle, NULL, attacker_task, (void*) &max_count);
   if (ret_a != 0)
   {   
      fprintf(stderr, "ERROR spawning thread...!\n"); 
      exit(EXIT_FAILURE);
   }

   // Victim thread
   victim_task( (void*) &max_count );

   // Join threads
   pthread_join(attacker_handle, NULL);

   SimDisableModels();
   printf("Done...! \n");
   return 0;
}

